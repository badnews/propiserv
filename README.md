# propiserv: TradeMe Property Search App #
------------------------------------------

Ravi Chandra / ravi@chandra.cc / 15 April 2015

## Objective  ##

Create a simple mobile app that demonstrates some familiarity with mobile development, tooling, and the ecosystem. Moreover, this should demonstrate *potential* to rapidly develop skills in this area.

### Constraints ###

The major issue is *time*, or lack thereof, since I've built this in one *night*. Ideally I would have liked to flesh this application out a bit more; and, in particular, considered porting to  Android!

### Features ###

* Uses **MapKit** and renders a scrollable map. I think this makes a more exciting change from a boring table :-)
* Follows a client/server architecture and uses **Alamofire** and **SwiftyJSON** to make HTTP requests to the TradeMe (sandbox) API service.
* Implements a basic TableView with custom cell layout. Probably one of the most common UI paradigms, I think?
* Asynchornous data/image fetching.
* Written in Swift. Bleeding edge.

## Usage ##

The app can be run, and starts up in the **Map tab**. This is positioned (hardcoded) on Wellington City. The visible markers are all the properties for sale on TradeMe.

*Note: this is talking to the sandbox API which has much less data than the production! This is why there are not many listings displayed!*

![initial map screen](/badnews/propiserv/raw/master/screenshot-1.png =300x)

The map is fully scrollable and zoomable. When the map region changes the app API client will make a new request to fetch the appropriate listings for that currently visible geographic region.

For example, after zooming out and scrolling up towards Auckland we arrive at the following screenshot; with a listing annotation on Waiheke Island selected.

![map mavigation and data refresh](/badnews/propiserv/raw/master/screenshot-2.png =300x)

The other main feature is the **Favourties tab** which is a simple table view of property listings. My intention was to add filtering/search capability to refine the visible listings, and then a detail page to expand on a listing but...

![favourites view](/badnews/propiserv/raw/master/screenshot-3.png =300x)

Once again, the listing data is fetched asynchronously from the API service and rendered within the custom TableViewCell.

## Future work ##

It would be nice to tidy up the code a bit and refactor some of the models and remove a bit of business logic from the view controllers.

Dealing with JSON data (and Swift optionals) seems to be a bit annoying -- maybe there are some better idioms/patterns to deal with this.

Obviously it would be nice to add some of the missing functionality, e.g. search filters!