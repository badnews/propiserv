//
//  FirstViewController.swift
//  propiserv
//
//  Created by Rav Chandra on 14/04/15.
//  Copyright (c) 2015 elysium. All rights reserved.
//

import MapKit
import UIKit

class MapController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
//    let startLocation = CLLocation(latitude: 51.50007773, longitude: -0.1246402)
    let startLocation = CLLocation(latitude: -41.286467, longitude: 174.776145)
    let tmApi = TradeMe()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        mapView.showsUserLocation = true
        mapView.pitchEnabled = false
        mapView.rotateEnabled = false

        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: startLocation.coordinate, span: span)
        mapView.setRegion(region, animated: true)
        mapView.delegate = self

    }

    func makeTMRegion() -> TMMapRegion {
        let x1 = MKMapRectGetMinX(mapView.visibleMapRect)
        let y1 = MKMapRectGetMinY(mapView.visibleMapRect)
        let x2 = MKMapRectGetMaxX(mapView.visibleMapRect)
        let y2 = MKMapRectGetMaxY(mapView.visibleMapRect)
        let bottomLeft = MKCoordinateForMapPoint(MKMapPointMake(x1, y2))
        let topRight = MKCoordinateForMapPoint(MKMapPointMake(x2, y1))
        return TMMapRegion(latMin: bottomLeft.latitude, latMax: topRight.latitude, longMin: bottomLeft.longitude, longMax: topRight.longitude)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func mapView(mapView: MKMapView!, regionDidChangeAnimated animated: Bool) {
        let tmRegion = makeTMRegion()
        tmApi.propertySearch(tmRegion, handler: annotateMapWithListings)
    }
    
    func annotateMapWithListings(properties: [TMPropertyInfo]) {
        mapView.removeAnnotations(mapView.annotations)
        for p in properties {
            let annotation = MKPointAnnotation()
            let loc = CLLocation(latitude: p.latitude, longitude: p.longitude)
            annotation.coordinate = loc.coordinate
            annotation.title = p.title
            annotation.subtitle = p.address
            mapView.addAnnotation(annotation)
        }
    }
}

