//
//  PropertyCell.swift
//  propiserv
//
//  Created by Rav Chandra on 14/04/15.
//  Copyright (c) 2015 elysium. All rights reserved.
//

import UIKit

class PropertyCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
}