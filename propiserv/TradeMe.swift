//
//  TradeMe.swift
//  propiserv
//
//  Created by Rav Chandra on 14/04/15.
//  Copyright (c) 2015 elysium. All rights reserved.
//

import Foundation
import Alamofire
import MapKit

struct TMMapRegion {
    var latMin = 0.0
    var latMax = 0.0
    var longMin = 0.0
    var longMax = 0.0
    
    init(latMin: Double, latMax: Double, longMin: Double, longMax: Double) {
        self.latMin = latMin
        self.latMax = latMax
        self.longMin = longMin
        self.longMax = longMax
    }
}

struct TMPropertyInfo {
    var title: String?
    var address: String?
    var price: String?
    var latitude: Double
    var longitude: Double
    var photoUri: String?
}

class TradeMe {
    // very very basic API client
    
    let propSearchUri = "https://api.tmsandbox.co.nz/v1/Search/Property/Residential.json"
    var manager = Alamofire.Manager.sharedInstance
    
    init() {
        // inject auth headers
        manager.session.configuration.HTTPAdditionalHeaders = [
            "Authorization": "OAuth oauth_consumer_key=\"8F126DA8955E7D219751DA1F81424C9F\", oauth_signature_method=\"PLAINTEXT\", oauth_signature=\"03C57B54FB574CC8AB7874715D903B13&\"",
        ]
    }

    func buildPropertyArray(jsonObj: JSON) -> [TMPropertyInfo] {
        var properties = [TMPropertyInfo]()
        for (_, l: JSON) in jsonObj["List"] {
            let property = TMPropertyInfo(title: l["Title"].string, address: l["Address"].string, price: l["PriceDisplay"].string, latitude: l["GeographicLocation"]["Latitude"].double!, longitude: l["GeographicLocation"]["Longitude"].double!, photoUri: l["PictureHref"].string)
            properties.append(property)
        }
        return properties
    }
    
    func propertySearch(region: TMMapRegion, handler: (properties: [TMPropertyInfo]) -> Void) {
        let params = [
            "photo_size": "list",
            "latitude_min": region.latMin,
            "latitude_max": region.latMax,
            "longitude_min": region.longMin,
            "longitude_max": region.longMax
        ]
        manager.request(.GET, propSearchUri, parameters: params as? [String : AnyObject]).responseJSON {
            (request, response, json, error) in
            let properties = self.buildPropertyArray(JSON(json!))
            handler(properties: properties)
        }
    }
    func propertySearch(district: Int, handler: (properties: [TMPropertyInfo]) -> Void) {
        let params = [
            "photo_size": "list",
            "district": district.description
        ]
        manager.request(.GET, propSearchUri, parameters: params).responseJSON {
            (request, response, json, error) in
            let properties = self.buildPropertyArray(JSON(json!))
            handler(properties: properties)
        }
    }
}
