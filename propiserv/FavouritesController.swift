//
//  SecondViewController.swift
//  propiserv
//
//  Created by Rav Chandra on 14/04/15.
//  Copyright (c) 2015 elysium. All rights reserved.
//

import UIKit
import Alamofire


class FavouritesController: UITableViewController {

    @IBOutlet var theTableView: UITableView!
    
    let district = 7  // Auckland City
    let tmApi = TradeMe()
    var properties = [TMPropertyInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tmApi.propertySearch(district, handler: {
            self.properties = $0
            self.theTableView.reloadData()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return properties.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 160
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("propertyCell") as! PropertyCell
        let row = indexPath.row
        if row < properties.count {
            let property = properties[indexPath.row]
            cell.title.text = property.title
            cell.price.text = property.price
            cell.address.text = property.address
            // async load the photos
            if (property.photoUri != nil) {
                Alamofire.request(.GET, property.photoUri!).response {
                    (request, response, data, error) in
                    cell.thumbnail.image = UIImage(data: data as! NSData)
                    return
                }
            }
        }
        return cell
    }

}

